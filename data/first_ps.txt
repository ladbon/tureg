// first_ps.txt

struct PixelInput 
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
};

Texture2D diffuseTexture;
SamplerState defaultSampler;

float4 main(PixelInput input) : SV_TARGET
{
	return diffuseTexture.Sample(defaultSampler, input.texcoord);
}
