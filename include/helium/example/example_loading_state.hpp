// example_loading_state.hpp

#ifndef EXAMPLELOADINGSTATE_HPP_INCLUDED
#define EXAMPLELOADINGSTATE_HPP_INCLUDED

#include <helium/gameplay/abstract_state.hpp>

namespace helium
{
	namespace resource
	{
		class Font;
	}

	namespace example
	{
		class ExampleLoadingState : public gameplay::AbstractState
		{
		public:
			ExampleLoadingState();
			~ExampleLoadingState();

			void initialize();
			void shutdown();
			bool update();
			void draw();
			uint32_t next();

		private:
			resource::Font* m_font;
			uint32_t m_vertex_buffer;
			uint32_t m_index_buffer;
			std::list<std::string> m_resources;

			struct Vertex
			{
				Vector3 position;
				Vector2 texcoord;
			};

			Vertex* m_vertices;
		};
	}
}

#endif // EXAMPLELOADINGSTATE_HPP_INCLUDED
