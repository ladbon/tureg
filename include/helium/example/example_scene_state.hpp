// example_scene_state.hpp

#ifndef EXAMPLESCENESTATE_HPP_INCLUDED
#define EXAMPLESCENESTATE_HPP_INCLUDED

#include <helium/gameplay/abstract_state.hpp>
#include <helium/scene/scene.hpp>

namespace helium
{
	namespace example
	{
		class ExampleSceneState : public gameplay::AbstractState
		{
		public:
			ExampleSceneState();
			~ExampleSceneState();

			void initialize();
			void shutdown();
			bool update();
			void draw();
			uint32_t next();

		private:
			scene::Scene::Ptr m_scene;
		};
	}
}

#endif // EXAMPLESTATE_HPP_INCLUDED
