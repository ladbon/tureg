//GameState.cpp

#include "stdafx.h"
#include "helium/states/GameState.h"
#include <helium/service_locator.hpp>
#include <helium/resource/resource_cache.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/scene/camera.hpp>
#include <helium/scene/transform.hpp>
#include <helium/scene/lighting_property.hpp>
#include <helium/scene/shader_unit.hpp>
#include <helium/scene/shader_parameter.hpp>
#include <helium/scene/texture_unit.hpp>
#include <helium/scene/technique.hpp>
#include <helium/scene/technique_factory.hpp>
#include <helium/example/example_scene_state.hpp>
#include <application.hpp>
#include <iostream>

namespace helium
{
	namespace State
	{
		Gamestate::Gamestate()
		{
			m_heightmap = nullptr;
			m_render_system = nullptr;
			m_audio_system = nullptr;
			m_sound_music = nullptr;
		}
		Gamestate::~Gamestate()
		{
			if (m_heightmap)
			{
				delete m_heightmap;
				m_heightmap = nullptr;
			}
			shutdown();
		}

		void Gamestate::initialize()
		{
			
			// CheckpointManager
			m_cpmanager = helium::CheckpointManager::create();


			// audio
			FMOD::Channel* chann = nullptr;
			m_audio_system = ServiceLocator<system::AudioSystem>::get_service();
			m_sound_music = m_audio_system->create_music("../data/music/Polka.mp3", chann, false, true);

			// Render
			m_render_system = ServiceLocator<system::RenderSystem>::get_service();
			
			// Create scene
			m_scene = scene::Scene::create();
			scene::SceneNode* node = m_scene->create_scene_node();
			(void)node;

			// HUD 
			m_hud = gameplay::HUD::create();
			m_hud->initialize();
			m_hud->set_GUI_HUD(1);
			m_hud->construct_HUD();
			m_hud->constructTimerrect();

			// Camera
			m_camera = scene::Camera::create();
			m_camera->set_perspective(Math::to_rad(45.0f), 1280.0f / 720.0f, 0.5f, 1000.0f);
			m_camera->set_Position(Vector3(105.870193f, 254.039780f, -121.995140f));
			m_camera->rotatex(-249.845f);
			m_camera->update();

			// Heightmap
			m_heightmap = new Heightmap(m_render_system);
			m_heightmap->initialize();
			sun = m_sun.initialize();

			// Player_car
			m_car.initialize();
			m_car.m_carBody.initialize();
			m_car.m_carBody.create_body();

			// Skybox
			m_skybox = Skybox(m_render_system);
			m_skybox.initialize();
		}
		void Gamestate::shutdown()
		{
			m_audio_system->stop(m_sound_music);
			m_audio_system->stop(m_car.m_sound_effect_acc);
			m_audio_system->stop(m_car.m_sound_effect_brake);
		}
		bool Gamestate::update()
		{
			m_hud->update(DeltaTime::GetDeltaTime());

			//Audiosystem
			m_audio_system->process();
			
			// Camera
			m_camera->update();

			// Car
			m_car.update();
			m_car.get_position();
			
			// Gamerules! CheckpointManager
			if (m_cpmanager->update(m_car.get_position()) == true
				|| m_hud->gametimerdone())
			{
				return false;
			}
			
			return true;
		}
		void Gamestate::draw()
		{
			// Draw skybox
			m_skybox.draw_skybox(*m_camera);

			// Draw heightmap and car
			m_heightmap->draw_heightmap(sun, *m_camera);
			m_car.draw_car(sun, *m_camera);

			// Draw hud
			m_hud->draw_Hud(m_hud->get_HUD());
			m_hud->draw_Hud(m_hud->get_timerrect());
			
		}
		
		uint32_t Gamestate::next()
		{
			return String::hash32("StartState", 10);

		}
	}
}