//GameState.h

#ifndef GameState_H_INCLUDED
#define GameState_H_INCLUDED


#include <helium/gameplay/abstract_state.hpp>
#include <helium/scene/scene.hpp>
#include <helium/gameplay/entity/2d/HUD.h>
#include <helium/gameplay/surroundings/Heightmap.h>
#include <helium/gameplay/surroundings/sun.h>
#include <helium/gameplay/entity/3d/Player_car.h>
#include <helium/gameplay/surroundings/skybox.h>
#include <helium/gameplay/entity/gamerules/CheckpointManager.h>
#include <helium/system/audio_system.hpp>


namespace helium
{
	namespace State
	{
		class Gamestate : public gameplay::AbstractState 
		{
		public:
			Gamestate();
			~Gamestate();

			void initialize();
			void shutdown();
			bool update();
			void draw();
			uint32_t next();

		private:

			// Scene 
			scene::Scene::Ptr m_scene;
			
			// HUD
			gameplay::HUD::Ptr m_hud;

			// CheckpointManager
			CheckpointManager::Ptr m_cpmanager;

			// Camera
			scene::Camera::Ptr m_camera;

			// Heightmap
			Heightmap* m_heightmap;
			
			// Renderer
			system::RenderSystem* m_render_system;

			// Lightning
			sun m_sun;
			sun::Lighting sun;
			
			// Player_car and skybox
			helium::player_car m_car;
			Skybox m_skybox;

			// AudioSystem
			system::AudioSystem* m_audio_system;

			system::AudioSystem::Sound* m_sound_music;
		};
	}
}

#endif // Gamestate_H_INCLUDED
