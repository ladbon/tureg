// StartState.cpp

#include "stdafx.h"
#include "helium/states/StartState.h"
#include <helium/resource/resource_cache.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/scene/transform.hpp>
#include <helium/scene/shader_unit.hpp>
#include <helium/scene/shader_parameter.hpp>
#include <helium/scene/texture_unit.hpp>
#include <helium/scene/technique.hpp>
#include <helium/scene/technique_factory.hpp>
#include <helium/example/example_scene_state.hpp>
#include <application.hpp>
#include <iostream>

namespace helium
{
	namespace State
	{
		StartState::StartState()
		{
			
		}

		StartState::~StartState()
		{
		}

		void StartState::initialize()
		{
			
			// Initialize mouse
			m_mouse->create();
			
			// Get OSEevent
			ServiceLocator<OSEventDispatcher>::get_service()->attach(this);


			// Get render
			m_render_system = ServiceLocator<system::RenderSystem>::get_service();
			
			
			// Initialize scene
			m_scene = scene::Scene::create();
			scene::SceneNode* node = m_scene->create_scene_node();
			(void)node;

			// Initialize HUD
			m_hud = gameplay::HUD::create();
			m_hud->initialize();
			m_hud->construct_HUD();
		}

		void StartState::shutdown()
		{
		}
		bool StartState::update()
		{
			if (mb)
			{
				if (mx > 499 && mx < 1001 && my > 99 && my < 601)
				{ 
					return false;
				}
			}

			return true;
			
		}

		void StartState::draw()
		{
			m_hud->draw_Hud(m_hud->get_HUD());
		}
		void StartState::notify(OSEvent* event)
		{
			if (event->m_type == EOSEventType::MouseMove)
			{
				mx = event->m_data.mouse.move.x;
				my = event->m_data.mouse.move.y;
			}
			else if (event->m_type == EOSEventType::MouseButton)
			{
				if (event->m_data.mouse.button.button == EMouseButton::Left)
				{
					mb = event->m_data.mouse.button.state;
				}
			}
		}
		uint32_t StartState::next()
		{
				ServiceLocator<OSEventDispatcher>::get_service()->detach(this);
				return String::hash32("GameState", 9);
		}
	}
}
