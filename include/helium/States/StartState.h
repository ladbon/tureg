// example_loading_state.hpp

#ifndef StartState_H_INCLUDED
#define StartState_H_INCLUDED

#include <helium/gameplay/abstract_state.hpp>
#include <helium/gameplay/entity/2d/HUD.h>
#include <helium/scene/scene.hpp>
#include <helium/mouse.hpp>

namespace helium
{
	namespace State
	{
		class StartState : public gameplay::AbstractState, public OSEventListener
		{
		public:
			StartState();
			~StartState();

			void initialize();
			void shutdown();
			bool update();
			void draw();
			uint32_t next();
			void notify(OSEvent* event);

		private:

			//HUD
			gameplay::HUD::Ptr m_hud;
			system::RenderSystem* m_render_system;

			//Scene 
			scene::Scene::Ptr m_scene;

			//Mouse controls
			helium::Mouse::Ptr m_mouse;
			bool mb;
			int mx, my;

			//State
		};
	}
}

#endif // StartState_HP_INCLUDED
