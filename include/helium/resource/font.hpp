// font.hpp

#ifndef FONT_HPP_INCLUDED
#define FONT_HPP_INCLUDED

#include <helium/resource/resource.hpp>

namespace helium
{
	namespace resource
	{
		struct Glyph
		{
			uint32_t width, height;
			Vector4 texcoord;
		};

		class Texture;

		class Font : public Resource
		{
		private: // non-copyable
			Font(const Font&);
			Font& operator=(const Font&);

		public:
			typedef std::unique_ptr<Font> Ptr;

			static Ptr create(const std::string& filename);

		private:
			Font();

		public:
			~Font();

			bool has_glyph(uint32_t unicode) const;
			const Glyph& get_glyph(uint32_t unicode) const;

			bool has_texture() const;
			void set_texture(Texture* texture);
			const Texture* get_texture() const;

			void dispose() {}

		private:
			std::unordered_map<uint32_t, Glyph> m_glyphs;
			Texture* m_texture;
		};
	}
}

#endif // FONT_HPP_INCLUDED
