// model.hpp

#ifndef MODEL_HPP_INCLUDED
#define MODEL_HPP_INCLUDED

#include <helium/resource/resource.hpp>

namespace helium
{
	namespace system
	{
		enum class EBufferMode : uint32_t;
	}

	namespace scene
	{
		class Scene;
		class SceneNode;
	}

	namespace resource
	{
		class VertexBuffer : public Resource
		{
			friend class Mesh;
		private: // non-copyable
			VertexBuffer(const VertexBuffer&);
			VertexBuffer& operator=(const VertexBuffer&);

		public:
			typedef std::unique_ptr<VertexBuffer> Ptr;

			static Ptr create();

		private:
			VertexBuffer();

		public:
			~VertexBuffer();

			bool create(system::EBufferMode mode, uint16_t size, uint32_t count);

			uint16_t get_size() const;
			uint32_t get_count() const;
			const void* get_buffer() const;

			void dispose();

		private:
			system::EBufferMode m_mode;
			uint16_t	m_size;
			uint32_t	m_count;
			uint8_t*	m_buffer;
		};

		class IndexBuffer : public Resource
		{
			friend class Mesh;
		private: // non-copyable
			IndexBuffer(const IndexBuffer&);
			IndexBuffer& operator=(const IndexBuffer&);

		public:
			typedef std::unique_ptr<IndexBuffer> Ptr;

			static Ptr create();

		private:
			IndexBuffer();

		public:
			~IndexBuffer();

			uint16_t get_size() const;
			uint32_t get_count() const;
			const void* get_buffer() const;

			void dispose();

		private:
			uint16_t	m_size;
			uint32_t	m_count;
			void*			m_buffer;
		};

		class Mesh
		{
			friend class Model;
		private: // non-copyable
			Mesh(const Mesh&);
			Mesh& operator=(const Mesh&);

		public:
			typedef std::unique_ptr<Mesh> Ptr;

			static Ptr create();

		private:
			Mesh();

		public: 
			~Mesh();

			const VertexBuffer* get_vertex_buffer() const;

			bool has_index_buffer() const;
			const IndexBuffer* get_index_buffer() const;

		private:
			VertexBuffer::Ptr m_vertex_buffer;
			IndexBuffer::Ptr m_index_buffer;
		};

		class Model
		{
			friend class ResourceCache;
			friend class scene::Scene;
			friend class scene::SceneNode;
		private: // non-copyable
			Model(const Model&);
			Model& operator=(const Model&);

		public:
			typedef std::unique_ptr<Model> Ptr;

			static Ptr create(const std::string& filename);

		private:
			Model();

		public:
			~Model();

			uint32_t get_mesh_count() const;
			const Mesh* get_mesh(uint32_t index) const;

		private:
			void get_mesh_vertex_buffers(std::vector<uint32_t>& vertex_buffers);

		private:
			std::vector<Mesh::Ptr> m_meshes;
		};
	}
}

#endif // MODEL_HPP_INCLUDED
