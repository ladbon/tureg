// vertex_format.hpp

#ifndef VERTEXFORMAT_HPP_INCLUDED
#define VERTEXFORMAT_HPP_INCLUDED

#include <helium/resource/resource.hpp>

namespace helium
{
	namespace system
	{
		struct VertexFormatDesc;
	}

	namespace resource
	{
		// note(tommi): it is strongly recommended that you create this resource manually
		class VertexFormat : public Resource
		{
		private: // non-copyable
			VertexFormat(const VertexFormat&);
			VertexFormat& operator=(const VertexFormat&);

		public: 
			typedef std::unique_ptr<VertexFormat> Ptr;

			static Ptr create(const std::string& filename);

		private:
			VertexFormat();

		public:
			~VertexFormat();

			uint32_t get_count() const;
			const system::VertexFormatDesc* get_desc() const;

			void dispose() {}

		private:
			uint32_t m_count;
			system::VertexFormatDesc* m_desc;
		};
	}
}

#endif // VERTEXFORMAT_HPP_INCLUDED
