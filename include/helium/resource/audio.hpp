// audio.hpp

#ifndef AUDIO_HPP_INCLUDED
#define AUDIO_HPP_INCLUDED

#include <helium/resource/resource.hpp>

namespace helium
{
	namespace resource
	{
		enum class EAudioType : uint32_t
		{
			Mono,
			Stereo,
			Invalid,
		};

		// Important: This class only handles .ogg files!
		class Audio : public Resource
		{
		private: // non-copyable
			Audio(const Audio&);
			Audio& operator=(const Audio&);

		public:
			typedef std::unique_ptr<Audio> Ptr;

			static Ptr create(const std::string& filename);

		private:
			Audio();

		public:
			~Audio();

			bool is_valid() const;
			EAudioType get_type() const;
			uint32_t get_size() const;
			const void* get_stream() const;

			void dispose() {}

		private:
			EAudioType m_type;
			uint32_t m_size;
			int16_t* m_stream;
		};
	}
}

#endif // AUDIO_HPP_INCLUDED
