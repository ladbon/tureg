// shader.hpp

#ifndef SHADER_HPP_INCLUDED
#define SHADER_HPP_INCLUDED

#include <helium/resource/resource.hpp>

namespace helium
{
	namespace resource
	{
		class Shader : public Resource
		{
		private: // non-copyable
			Shader(const Shader&);
			Shader& operator=(const Shader&);

		public:
			typedef std::unique_ptr<Shader> Ptr;

			static Ptr create(const std::string& filename);

		private:
			Shader(const std::string& vertex, const std::string& pixel);

		public:
			~Shader();

			bool is_valid() const;

			const std::string& get_vertex_source() const;
			const std::string& get_pixel_source() const;

			void dispose();

		private:
			std::string m_vertex;
			std::string m_pixel;
		};
	}
}

#endif // SHADER_HPP_INCLUDED
