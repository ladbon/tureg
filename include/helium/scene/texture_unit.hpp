// texture_unit.hpp

#ifndef TEXTUREUNIT_HPP_INCLUDED
#define TEXTUREUNIT_HPP_INCLUDED

namespace helium
{
	namespace scene
	{
		class TextureUnit
		{
		private: // non-copyable
			TextureUnit(const TextureUnit&);
			TextureUnit& operator=(const TextureUnit&);

		public:
			typedef std::unique_ptr<TextureUnit> Ptr;

			static Ptr create(uint32_t texture, uint32_t sampler, uint32_t blend);

		private:
			TextureUnit(uint32_t texture, uint32_t sampler, uint32_t blend);

		public:
			~TextureUnit();

			uint32_t get_texture() const;
			uint32_t get_sampler() const;
			uint32_t get_blend_state() const;

		private:
			uint32_t m_texture;
			uint32_t m_sampler;
			uint32_t m_blend;
		};
	}
}

#endif // TEXTUREUNIT_HPP_INCLUDED
