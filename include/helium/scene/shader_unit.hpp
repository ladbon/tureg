// shader_unit.hpp

#ifndef SHADERUNIT_HPP_INCLUDED
#define SHADERUNIT_HPP_INCLUDED

#include <helium/scene/shader_parameter.hpp>

namespace helium
{
	namespace scene
	{
		class Camera;

		class ShaderUnit
		{
			friend class Scene;
		private: // non-copyable
			ShaderUnit(const ShaderUnit&);
			ShaderUnit& operator=(const ShaderUnit&);

		public:
			typedef std::unique_ptr<ShaderUnit> Ptr;

			static Ptr create();

		private:
			ShaderUnit();

		public:
			~ShaderUnit();

			uint32_t get_shader() const;
			uint32_t get_vertex_format() const;

			bool has_shader_parameter(const std::string& name) const;
			void set_shader_parameter(const std::string& name, uint32_t size, const void* buffer);
			const ShaderParameter* get_shader_parameter(const std::string& name) const;

		private:
			// note(tommi): called by scene when we set a camera 
			// automagically re-sets the projection and view 
			// for shader unit
			void on_camera_change(Camera* camera);
			void release_parameter(const char* name);

		private:
			uint32_t m_shader;
			uint32_t m_vertex_format;
			std::unordered_map<uint32_t, ShaderParameter::Ptr> m_parameters;
		};
	}
}

#endif // SHADERPROGRAM_HPP_INCLUDED
