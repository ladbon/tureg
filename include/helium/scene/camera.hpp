// camera.hpp

#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

namespace helium
{
	namespace scene
	{
		class Camera
		{
		private:// non-copyable
			Camera(const Camera&);
			Camera& operator=(const Camera&);

		public:
			typedef std::unique_ptr<Camera> Ptr;

			static Ptr create();

		private:
			Camera();

		public:
			~Camera();

			void update();

			void move_forward(float amount);
			void move_sidestep(float amount);
			void move_elevate(float amount);
			void rotatex(float amount);
			void rotatey(float amount);
			void rotatez(float amount);

			void set_perspective(float fov, float aspect, float znear, float zfar);
			void set_orthogonal(float width, float height, float znear, float zfar);

			const Matrix4& get_view() const;
			const Matrix4& get_projection() const;
			Vector3 get_position() const;

			const Frustum& get_frustum() const;

			// Added by Ladbon
			void set_Position(Vector3 p_position);


		private:
			bool m_dirty;
			float m_pitch;
			float m_yaw;
			float m_roll;

			Vector3 m_right;
			Vector3 m_up;
			Vector3 m_forward;
			Vector3 m_position;

			Matrix4 m_view;
			Matrix4 m_projection;

			Frustum m_frustum;
		};
	}
}

#endif // CAMERA_HPP_INCLUDED
