// keyboard.hpp

#ifndef KEYBOARD_HPP_INCLUDED
#define KEYBOARD_HPP_INCLUDED

#include <helium/os_event_listener.hpp>

namespace helium
{
	enum class EKeyCode : uint16_t 
	{
		Tab = 0x09,
		Enter = 0x0D,
		Shift = 0x10,
		Control = 0x11,
		Escape = 0x1B,
		Space = 0x20,
		Left = 0x25,
		Up = 0x26,
		Right = 0x27,
		Down = 0x28,
		_0 = 0x30,
		_1 = 0x31,
		_2 = 0x32,
		_3 = 0x33,
		_4 = 0x34,
		_5 = 0x35,
		_6 = 0x36,
		_7 = 0x37,
		_8 = 0x38,
		_9 = 0x39,
		_A = 0x41,
		_B = 0x42,
		_C = 0x43,
		_D = 0x44,
		_E = 0x45,
		_F = 0x46,
		_G = 0x47,
		_H = 0x48,
		_I = 0x49,
		_J = 0x4A,
		_K = 0x4B,
		_L = 0x4C,
		_M = 0x4D,
		_N = 0x4E,
		_O = 0x4F,
		_P = 0x50,
		_Q = 0x51,
		_R = 0x52,
		_S = 0x53,
		_T = 0x54,
		_U = 0x55,
		_V = 0x56,
		_W = 0x57,
		_X = 0x58,
		_Y = 0x59,
		_Z = 0x5A,
		COUNT = 256,
		UNICODECOUNT = 32,
	};

	class Keyboard : OSEventListener
	{
	private: // non-copyable
		Keyboard(const Keyboard&);
		Keyboard& operator=(const Keyboard&);

	public:
		typedef std::unique_ptr<Keyboard> Ptr;

		static Ptr create();

	private:
		Keyboard();

	public:
		~Keyboard();

		bool is_key_down(EKeyCode key) const;
		bool is_key_down_once(EKeyCode key) const;

		void post_frame();

	private:
		void notify(OSEvent* event);

	private:
		bool m_curr[(uint32_t)EKeyCode::COUNT];
		bool m_prev[(uint32_t)EKeyCode::COUNT];
	};
}

#endif // KEYBOARD_HPP_INCLUDED
