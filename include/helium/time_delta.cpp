//time_delta.cpp

#include "stdafx.h"
#include <helium\time_delta.h>

namespace helium
{
	helium::Time DeltaTime::m_xNow = helium::Time();
	helium::Time DeltaTime::m_xDeltaTime = helium::Time();
	helium::Time DeltaTime::m_xTimePrev = helium::Time();
	float DeltaTime::m_fDeltaTime = 0.0f;

	void DeltaTime::Update()
	{
		m_xNow = helium::Time::now();
		m_xDeltaTime = m_xNow - m_xTimePrev;
		m_xTimePrev = m_xNow;

		m_fDeltaTime = m_xDeltaTime.as_seconds();
	}

	float DeltaTime::GetDeltaTime()
	{
		return m_fDeltaTime;
	}
}