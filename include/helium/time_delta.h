//time_delta.h

#pragma once
#include <helium\time.hpp>

namespace helium
{
	class DeltaTime
	{
	private:
		DeltaTime(){};

	public:
		static void Update();
		static float GetDeltaTime();

	private:
		static helium::Time m_xNow;
		static helium::Time m_xDeltaTime;
		static helium::Time m_xTimePrev;
		static float m_fDeltaTime;
	};
}