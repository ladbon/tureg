// state_manager.hpp

#ifndef STATEMANAGER_HPP_INCLUDED
#define STATEMANAGER_HPP_INCLUDED

#include <helium/gameplay/abstract_state.hpp>
#include <helium/gameplay/state_factory.hpp>

namespace helium
{
	namespace gameplay
	{
		class StateManager
		{
		private: // non-copyable
			StateManager(const StateManager&);
			StateManager& operator=(const StateManager&);

		public:
			typedef std::unique_ptr<StateManager> Ptr;

			static Ptr create();

		private:
			StateManager();

		public:
			~StateManager();

			virtual void update();
			virtual void draw();

			void set_state(uint32_t id_hash);
			void set_factory(StateFactory::Ptr factory);
			StateFactory* get_factory();

		private:
			AbstractState* m_active_state;
			StateFactory::Ptr m_factory;
		};
	}
}

#endif // STATEMANAGER_HPP_INCLUDED
