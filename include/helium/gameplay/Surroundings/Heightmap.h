//Heightmap.h

//Note:(orre) this is the heightmap, the race circuit.

#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H


#include <helium/math.hpp>
#include <helium/resource/texture.hpp>
#include <helium/system/render_system.hpp>
#include <helium/gameplay/surroundings/sun.h>
#include <helium/scene/camera.hpp>
#include <fstream>

namespace helium
{

	/*namespace gameplay
	{*/
	class Heightmap
	{
	public:
		Heightmap(system::RenderSystem* rend);
		~Heightmap(){}

		bool initialize();

		void draw_heightmap(helium::sun::Lighting&, helium::scene::Camera&);

		void cleanup();
//	private:
		resource::Texture* m_texture;
		system::RenderSystem* m_render_system;

		//Todo(orre) may be better to have these variables seperate as well because a lot of stuff are
		//using these things.
		struct Material
		{
			float ambient;
			float diffuse;
			float specular;
			float shininess;

		};
		

		int shader;
		int texture;
		int sampler;
		int format;
		int buffer;
		int index;
		int blend;
		int depth;
		int raster;
		int drawcount;
		Matrix4 world;
		Material material;
	};
}
//}


#endif