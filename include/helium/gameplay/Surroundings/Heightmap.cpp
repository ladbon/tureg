//Heightmap.cpp

#include "stdafx.h"
#include "Heightmap.h"

using namespace helium;
Heightmap::Heightmap(system::RenderSystem* rend)
{
	m_render_system = rend;
}
bool Heightmap::initialize()
{
	

	// read shaders from files
	std::ifstream stream;
	std::string vssrc;
	stream.open("../data/lighting_vs.txt", std::ios_base::ate);
	std::ifstream::pos_type pos = stream.tellg();
	stream.seekg(std::ios_base::beg);
	vssrc.resize(pos);
	stream.read(const_cast<char*>(vssrc.c_str()), pos);
	stream.close();

	std::string pssrc;
	stream.open("../data/lighting_ps.txt", std::ios_base::ate);
	pos = stream.tellg();
	stream.seekg(std::ios_base::beg);
	pssrc.resize(pos);
	stream.read(const_cast<char*>(pssrc.c_str()), pos);
	stream.close();

	shader = m_render_system->create_shader(vssrc.c_str(), pssrc.c_str());
	
	// create vertex format
	system::VertexFormatDesc desc[] =
	{
		{ 0, system::EVertexAttributeFormat::Position, system::EVertexAttributeType::Float, 3 },
		{ 0, system::EVertexAttributeFormat::TexCoord, system::EVertexAttributeType::Float, 2 },
		{ 0, system::EVertexAttributeFormat::Normal, system::EVertexAttributeType::Float, 3 },
	};

	format = m_render_system->create_vertex_format(desc, sizeof(desc) / sizeof(desc[0]), shader);
	
	// sampler state
	// Note:(orre) not sure about the last parameter
	sampler = m_render_system->create_sampler_state(system::ESamplerFilterMode::Linear,
		system::ESamplerAddressMode::Wrap, system::ESamplerAddressMode::Wrap, system::ESamplerAddressMode::Invalid);
	
	// texture
	resource::Texture::Ptr p_texture = resource::Texture::create("../data/sometexture2.png");
	texture = m_render_system->create_texture(
		p_texture->get_format(), p_texture->get_type(),
		p_texture->get_width(),
		p_texture->get_height(),
		p_texture->get_depth(),
		p_texture->get_pixel_data()
		);

	blend = m_render_system->create_blend_state(
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendOp::Add, system::EBlendOp::Add);
	
	world.identity();
	material.ambient = 0.1f;
	material.diffuse = 0.5f;
	material.specular = 0.5f;
	material.shininess = 32.0f;

	// create heightmap vertices and indices
	resource::Texture::Ptr heightmap = resource::Texture::create("../data/heightmap2.1.1.png");

	uint32_t width = heightmap->get_width();
	uint32_t height = heightmap->get_height();
	uint32_t components = 3;
	const uint8_t* pixels = heightmap->get_pixel_data();

	struct Vertex
	{
		Vector3 position;
		Vector2 texcoord;
		Vector3 normal;
	};

	Vertex* vertices = new Vertex[width * height];
	for (int32_t y = 0; y < (int32_t)height; y++)
	{
		for (int32_t x = 0; x < (int32_t)width; x++)
		{
			int index = y * width + x;
			float tx = float(x) / (float)width;
			float ty = float(y) / (float)height;
			float py = pixels[index * components] * 0.05f;
			vertices[index].position = Vector3(x, py, -y);
			vertices[index].texcoord = Vector2(tx, ty);
			vertices[index].normal = Vector3(0.0f, 0.0f, 0.0f);
		}
	}

	uint32_t count = (width - 1) * (height - 1) * 6;
	uint32_t* indices = new uint32_t[count];
	for (uint32_t y = 0, iidx = 0; y < (height - 1); y++)
	{
		for (uint32_t x = 0; x < (width - 1); x++)
		{
			indices[iidx++] = x + y      * width;
			indices[iidx++] = (x + 1) + y      * width;
			indices[iidx++] = (x + 1) + (y + 1) * width;

			indices[iidx++] = (x + 1) + (y + 1) * width;
			indices[iidx++] = x + (y + 1) * width;
			indices[iidx++] = x + y      * width;
		}
	}

	for (uint32_t i = 0; i < count; i += 3)
	{
		Vector3& p0 = vertices[indices[i + 0]].position;
		Vector3& p1 = vertices[indices[i + 1]].position;
		Vector3& p2 = vertices[indices[i + 2]].position;

		Vector3 q0 = p1 - p0;
		Vector3 q1 = p2 - p0;
		Vector3 n = q0.cross(q1);
		n.normalize();

		vertices[indices[i + 0]].normal += n;
		vertices[indices[i + 1]].normal += n;
		vertices[indices[i + 2]].normal += n;
	}

	for (uint32_t i = 0; i < width * height; i++)
	{
		vertices[i].normal.normalize();
	}

	buffer = m_render_system->create_vertex_buffer(
		system::EBufferMode::Static,
		vertices,
		sizeof(Vertex),
		width * height);
	delete[] vertices;
	vertices = nullptr;

	index = m_render_system->create_index_buffer(
		system::EIndexBufferType::Int32,  count, indices);
	delete[] indices;
	indices = nullptr;

	drawcount = count;

	//rasterizer state
	raster = m_render_system->create_rasterizer_state(
		system::EFillMode::Solid, system::ECullMode::None);

	depth = m_render_system->create_depth_state(true, true, system::EDepthMode::LessEqual);
	return true;
}

void Heightmap::draw_heightmap(helium::sun::Lighting& sun, helium::scene::Camera& camera)
{
	m_render_system->select_shader(shader);
	m_render_system->select_texture(texture);
	m_render_system->select_sampler_state(sampler);
	m_render_system->select_vertex_format(format);
	m_render_system->select_vertex_buffer(buffer);
	m_render_system->select_index_buffer(index);
	m_render_system->select_rasterizer_state(raster);
	m_render_system->select_blend_state(blend);
	m_render_system->select_depth_state(depth);
	m_render_system->set_shader_constant_matrix4("projection", camera.get_projection());
	m_render_system->set_shader_constant_matrix4("view", camera.get_view());
	m_render_system->set_shader_constant_matrix4("world", world);
	m_render_system->set_shader_constant_vector3("direction", sun.direction);
	m_render_system->set_shader_constant_vector3("diffuse", sun.diffuse);
	m_render_system->set_shader_constant_vector3("ambient", sun.ambient);
	m_render_system->set_shader_constant_vector3("eye", camera.get_position());
	m_render_system->set_shader_constant_raw("material", &material, sizeof(material));
	m_render_system->apply();
	//m_render_system->draw(EDrawMode::TriangleList, 0, 36);
	m_render_system->drawi(system::EDrawMode::TriangleList, 0, drawcount);
}

void Heightmap::cleanup()
{
	delete m_texture;
	delete m_render_system;
}