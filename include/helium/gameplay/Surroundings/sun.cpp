//sun.cpp

#include "stdafx.h"

#include <helium/gameplay/Surroundings/Sun.h>

helium::sun::sun()
{

}

helium::sun::~sun()
{

}

helium::sun::Lighting helium::sun::initialize()
{
	m_sun.direction = Vector3(0.0f, -1.0f, 1.0);
	m_sun.direction.normalize();
	m_sun.diffuse = Vector3(1.0f, 1.0f, 1.0f);
	m_sun.ambient = Vector3(1.0f, 1.0f, 1.0f);
	m_sun.eye = Vector3(0.0f, 0.0f, -6.0f);
	m_sun.pad0 = m_sun.pad1 = m_sun.pad2 = m_sun.pad3 = 1.0f;

	return m_sun;
}