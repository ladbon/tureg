//skybox.cpp

#include "stdafx.h"

#include <helium/gameplay/surroundings/skybox.h>


helium::Skybox::Skybox(system::RenderSystem* rend)
{
	m_render_system = rend;
}

helium::Skybox::~Skybox()
{

}

void helium::Skybox::initialize()
{
	//skybox = {};

	resource::Shader::Ptr shader = resource::Shader::create("../data/skybox/skybox.shader.txt");
	m_shader = m_render_system->create_shader(shader->get_vertex_source().c_str(),
		shader->get_pixel_source().c_str());

	resource::VertexFormat::Ptr format = resource::VertexFormat::create(
		"../data/skybox/skybox.vertexformat.txt");
	m_format = m_render_system->create_vertex_format(
		const_cast<system::VertexFormatDesc*>(format->get_desc()),
		format->get_count(),
		m_shader);

	resource::Texture::Ptr texture = resource::Texture::create(
		"../data/garden_sky.png");
	m_texture = m_render_system->create_texture(
		texture->get_format(),
		texture->get_type(),
		texture->get_width(),
		texture->get_height(),
		texture->get_depth(),
		texture->get_pixel_data());

	resource::Sampler::Ptr sampler = resource::Sampler::create(
		"../data/skybox/skybox.sampler.txt");
	m_sampler = m_render_system->create_sampler_state(
		sampler->get_filter_mode(),
		sampler->get_addr_u(),
		sampler->get_addr_v(),
		sampler->get_addr_w());

	m_raster = m_render_system->create_rasterizer_state(
		system::EFillMode::Solid,
		system::ECullMode::Front);

	m_depth = m_render_system->create_depth_state(
		false, false, system::EDepthMode::Less);

	struct Vertex
	{
		Vector3 position;
		Vector2 texcoord;
	};

	m_drawcount = 36;
	float v = 1.0f;
	float y_left = 256.f / 768.f;
	float oneminus_y_left = 1.f - (y_left);
	float x_left = 254.f / 1024.f;
	float x_left_front = 510.f / 1024.f;
	float x_right = x_left_front;
	float x_right_back = 766.f / 1024.f;
	float x_back = x_right_back;

	Vertex vertices[] =
	{
		// front
		{ Vector3(-v, v, -v), Vector2(x_left, y_left) },
		{ Vector3(v, v, -v), Vector2(x_left_front, y_left) },
		{ Vector3(v, -v, -v), Vector2(x_left_front, oneminus_y_left) },
		{ Vector3(v, -v, -v), Vector2(x_left_front, oneminus_y_left) },
		{ Vector3(-v, -v, -v), Vector2(x_left, oneminus_y_left) },
		{ Vector3(-v, v, -v), Vector2(x_left,y_left) },

		// right
		{ Vector3(v, v, -v), Vector2(x_right, y_left) },
		{ Vector3(v, v, v), Vector2(x_right_back, y_left) },
		{ Vector3(v, -v, v), Vector2(x_right_back, oneminus_y_left) },
		{ Vector3(v, -v, v), Vector2(x_right_back, oneminus_y_left) },
		{ Vector3(v, -v, -v), Vector2(x_right, oneminus_y_left) },
		{ Vector3(v, v, -v), Vector2(x_right, y_left) },

		// back
		{ Vector3(v, v, v), Vector2(x_back, y_left) },
		{ Vector3(-v, v, v), Vector2(1.f, y_left) },
		{ Vector3(-v, -v, v), Vector2(1.f, oneminus_y_left) },
		{ Vector3(-v, -v, v), Vector2(1.f, oneminus_y_left) },
		{ Vector3(v, -v, v), Vector2(x_back, oneminus_y_left) },
		{ Vector3(v, v, v), Vector2(x_back, y_left) },

		// left
		{ Vector3(-v, v, v), Vector2(0.f, y_left) },
		{ Vector3(-v, v, -v), Vector2(x_left, y_left) },
		{ Vector3(-v, -v, -v), Vector2(x_left, oneminus_y_left) },
		{ Vector3(-v, -v, -v), Vector2(x_left, oneminus_y_left) },
		{ Vector3(-v, -v, v), Vector2(0.f, oneminus_y_left) },
		{ Vector3(-v, v, v), Vector2(0, y_left) },

		// top
		{ Vector3(-v, v, v), Vector2(x_left, 0.f) },
		{ Vector3(v, v, v), Vector2(x_left_front, 0.f) },
		{ Vector3(v, v, -v), Vector2(x_left_front, y_left) },
		{ Vector3(v, v, -v), Vector2(x_left_front, y_left) },
		{ Vector3(-v, v, -v), Vector2(x_left, y_left) },
		{ Vector3(-v, v, v), Vector2(x_left, 0.f) },

		// bottom
		{ Vector3(-v, -v, -v), Vector2(x_left, oneminus_y_left) },
		{ Vector3(v, -v, -v), Vector2(x_left_front, oneminus_y_left) },
		{ Vector3(v, -v, v), Vector2(x_left_front, 1.0f) },
		{ Vector3(v, -v, v), Vector2(x_left_front, 1.0f) },
		{ Vector3(-v, -v, v), Vector2(x_left, 1.0f) },
		{ Vector3(-v, -v, -v), Vector2(x_left, oneminus_y_left) },
	};

	m_buffer = m_render_system->create_vertex_buffer(
		system::EBufferMode::Static,
		vertices,
		sizeof(Vertex),
		sizeof(vertices) / sizeof(vertices[0]));
}

void helium::Skybox::draw_skybox(scene::Camera& camera)
{
	Matrix4 view = camera.get_view();							
	view.m._41 = 0.0f;
	view.m._42 = 0.0f;
	view.m._43 = 0.0f;

	Matrix4 projection = camera.get_projection();
	projection.m._41 = 0.0f;
	projection.m._42 = 0.0f;
	projection.m._43 = 0.0f;

	m_render_system->select_shader(m_shader);
	m_render_system->select_texture(m_texture);
	m_render_system->select_sampler_state(m_sampler);
	m_render_system->select_vertex_format(m_format);
	m_render_system->select_vertex_buffer(m_buffer);
	m_render_system->select_depth_state(m_depth);
	m_render_system->select_rasterizer_state(m_raster);
	m_render_system->set_shader_constant_matrix4("projection", camera.get_projection());//projection
	m_render_system->set_shader_constant_matrix4("view", view);//camera->get_view()
	m_render_system->apply();
	m_render_system->draw(system::EDrawMode::TriangleList, 0, 36);//skybox.drawcount
}