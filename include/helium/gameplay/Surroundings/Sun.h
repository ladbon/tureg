//sun.h

#ifndef SUN_H
#define SUN_H

#include <helium/math.hpp>
namespace helium
{
	class sun
	{
	public:
		struct Lighting
		{
			Vector3 direction; float pad0;
			Vector3 diffuse; float pad1;
			Vector3 ambient; float pad2;
			Vector3 eye; float pad3;
		};
		sun();
		~sun();

		Lighting initialize();

		Lighting m_sun;
	};
}


#endif