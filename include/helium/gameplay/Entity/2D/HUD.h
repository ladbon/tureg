//HUD.h

#ifndef HUD_H_INCLUDED
#define HUD_H_INCLUDED

#include <helium/gameplay/abstract_game.hpp>
#include <helium/scene/camera.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>
#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>

namespace helium
{
	namespace gameplay
	{

#pragma pack(push, 1)
		struct UIElement
		{
			int shader;
			int texture;
			int sampler;
			int format;
			int buffer;
			int index;
			int blend;
			int depth;
			int raster;
			float x, y, w, h;
			Matrix4 world;
		};
#pragma pack(pop)

		class HUD
		{
		public:
			HUD();
			~HUD();

			typedef std::unique_ptr<HUD> Ptr;

			static Ptr create();

			void initialize();
			void construct_HUD();
			void constructTimerrect();
			void update(const float &p_DeltaTime);
			void draw_Hud(UIElement p_hud);
			void set_GUI_HUD(int p_pic);
			UIElement get_HUD();
			UIElement get_timerrect();
			bool gametimerdone();

		private:
			struct Timer
			{
				Timer(const float &p_max)
				{
					m_max = p_max;
					m_current = m_max;
				}
				void Update(const float &p_deltaTime)
				{
					m_current -= p_deltaTime;
					if (m_current <= 0.0f)
					{
						m_current = 0.0f;
					}
				}
				bool Done()
				{
					return m_current <= 0.0f;
				}
				float GetPercentageDone()
				{
					return m_current / m_max;
				}
				float m_max;
				float m_current;
			} m_gametimer;

			UIElement m_hud;
			UIElement m_timerrect;
			float m_timerrect_scale;
			system::RenderSystem* m_render_system;
			std::string m_set;
			int GUI_HUD;
		};
	}
}
#endif