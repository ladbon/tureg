//HUD.cpp

#include "stdafx.h"
#include "helium/gameplay/Entity/2D/HUD.h"
#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>
#include <helium/system/collision_system.hpp>
#include <helium/system/render_system.hpp>
#include <helium/gameplay/abstract_game.hpp>
#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>
#include <helium/scene/camera.hpp>

#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>

#define GAMETIMER 60.0f

namespace helium
{
	namespace gameplay
	{
		using namespace helium::system;

		// static 
		HUD::Ptr HUD::create()
		{
			return HUD::Ptr(new HUD);
		}

		HUD::HUD()
			: m_gametimer(GAMETIMER)
		{
			GUI_HUD = 0;
			m_set = "../data/gui/start.png";
		}
		HUD::~HUD()
		{

		}
		void HUD::initialize()
		{
			m_render_system = ServiceLocator<system::RenderSystem>::get_service();
			m_timerrect_scale = 1.0f;
		}
		void HUD::set_GUI_HUD(int p_pic)
		{
			if (p_pic == 1)
			{
				GUI_HUD = 1;
				m_set = "../data/gui/gui.png";
				return;
			}

			GUI_HUD = 0;
			m_set = "../data/gui/start.png";

		}
		void HUD::construct_HUD()
		{
			resource::Shader::Ptr shader = resource::Shader::create("../data/gui/gui.shader.txt");
			m_hud.shader = m_render_system->create_shader(
				shader->get_vertex_source().c_str(),
				shader->get_pixel_source().c_str());

			resource::Texture::Ptr texture = resource::Texture::create(m_set);
			m_hud.texture = m_render_system->create_texture(
				texture->get_format(),
				texture->get_type(),
				texture->get_width(),
				texture->get_height(),
				texture->get_depth(),
				texture->get_pixel_data());

			resource::Sampler::Ptr sampler = resource::Sampler::create("../data/gui/gui.sampler.txt");
			m_hud.sampler = m_render_system->create_sampler_state(
				sampler->get_filter_mode(),
				sampler->get_addr_u(),
				sampler->get_addr_v(),
				sampler->get_addr_w());

			resource::VertexFormat::Ptr format = resource::VertexFormat::create("../data/gui/gui.vertexformat.txt");
			m_hud.format = m_render_system->create_vertex_format(
				const_cast<system::VertexFormatDesc*>(format->get_desc()),
				format->get_count(),
				m_hud.shader);

			if (GUI_HUD == 0)
			{
				m_hud.x = 500.0f;
				m_hud.y = 100.0f;
				m_hud.w = 500.f;
				m_hud.h = 500.f;
			}
			else if (GUI_HUD == 1)
			{
				m_hud.x = 0.0f;
				m_hud.y = 0.0f;
				m_hud.w = 1200.f;
				m_hud.h = 700.f;
			}

			struct Vertex
			{
				Vector3 position;
				Vector2 texcoord;
			}
			vertices[] =
			{
				{ Vector3(m_hud.x, m_hud.y, 0.5f), Vector2(0.0f, 0.0f) },
				{ Vector3(m_hud.x + m_hud.w, m_hud.y, 0.5f), Vector2(1.0f, 0.0f) },
				{ Vector3(m_hud.x + m_hud.w, m_hud.y + m_hud.h, 0.5f), Vector2(1.0f, 1.0f) },
				{ Vector3(m_hud.x, m_hud.y + m_hud.h, 0.5f), Vector2(0.0f, 1.0f) },
			};

			m_hud.buffer = m_render_system->create_vertex_buffer(
				system::EBufferMode::Static,
				vertices, sizeof(Vertex),
				sizeof(vertices) / sizeof(vertices[0]));

			UINT32 indices[] = { 0, 1, 2, 2, 3, 0 };
			m_hud.index = m_render_system->create_index_buffer(system::EIndexBufferType::Int32,
				sizeof(indices) / sizeof(indices[0]),
				indices);


			m_hud.blend = m_render_system->create_blend_state(
				system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha,
				system::EBlendMode::One, system::EBlendMode::One,
				system::EBlendOp::Add, system::EBlendOp::Add);

			m_hud.depth = m_render_system->create_depth_state(false, false, system::EDepthMode::Always);
			m_hud.raster = m_render_system->create_rasterizer_state(EFillMode::Solid, ECullMode::None);

			m_hud.world.identity();
			m_hud.world = m_hud.world * Matrix4::translation(Vector3(1.0f, 1.0f, 0.5f));
		}

		void HUD::constructTimerrect()
		{
			m_timerrect.shader = m_hud.shader;

			resource::Texture::Ptr texture = resource::Texture::create("../data/timerrect.png");
			m_timerrect.texture = m_render_system->create_texture(
				texture->get_format(),
				texture->get_type(),
				texture->get_width(),
				texture->get_height(),
				texture->get_depth(),
				texture->get_pixel_data());

			m_timerrect.sampler = m_hud.sampler;
			m_timerrect.format = m_hud.format;

			m_timerrect.x = 0.0f;
			m_timerrect.y = 0.0f;
			m_timerrect.w = 800.0f;
			m_timerrect.h = 10.0f;

			struct Vertex
			{
				Vector3 position;
				Vector2 texcoord;
			}
			vertices[] =
			{
				{ Vector3(m_timerrect.x, m_timerrect.y, 0.5f), Vector2(0.0f, 0.0f) },
				{ Vector3(m_timerrect.x + m_timerrect.w, m_timerrect.y, 0.5f), Vector2(1.0f, 0.0f) },
				{ Vector3(m_timerrect.x + m_timerrect.w, m_timerrect.y + m_timerrect.h, 0.5f), Vector2(1.0f, 1.0f) },
				{ Vector3(m_timerrect.x, m_timerrect.y + m_timerrect.h, 0.5f), Vector2(0.0f, 1.0f) },
			};

			m_timerrect.buffer = m_render_system->create_vertex_buffer(
				system::EBufferMode::Static,
				vertices, sizeof(Vertex),
				sizeof(vertices) / sizeof(vertices[0]));

			m_timerrect.index = m_hud.index;
			m_timerrect.blend = m_hud.blend;
			m_timerrect.depth = m_hud.depth;
			m_timerrect.raster = m_hud.raster;

			m_timerrect.world.identity();
			m_timerrect.world = m_timerrect.world * Matrix4::translation(Vector3(150.0f, 20.0f, 0.5f));
		}

		void HUD::update(const float &p_deltaTime)
		{
			m_gametimer.Update(p_deltaTime);

			m_timerrect.world.identity();
			m_timerrect.world = m_timerrect.world * Matrix4::scale(Vector3(m_gametimer.GetPercentageDone(), 1.0f, 1.0f)) * Matrix4::translation(Vector3(250.0f, 20.0f, 0.5f));
		}

		void HUD::draw_Hud(UIElement p_hud)
		{
			Matrix4 ortho = Matrix4::orthogonal(1280.0f, 720.0f, 0.0f, 1.0f);

			m_render_system->select_shader(p_hud.shader);
			m_render_system->select_texture(p_hud.texture);
			m_render_system->select_sampler_state(p_hud.sampler);
			m_render_system->select_vertex_format(p_hud.format);
			m_render_system->select_vertex_buffer(p_hud.buffer);
			m_render_system->select_index_buffer(p_hud.index);
			m_render_system->select_blend_state(p_hud.blend);
			m_render_system->select_depth_state(p_hud.depth);
			m_render_system->select_rasterizer_state(p_hud.raster);
			m_render_system->set_shader_constant_matrix4("projection", ortho);
			m_render_system->set_shader_constant_matrix4("world", p_hud.world);
			m_render_system->apply();

			m_render_system->drawi(system::EDrawMode::TriangleList, 0, 6);
		}

		bool HUD::gametimerdone()
		{
			return m_gametimer.Done();
		}

		UIElement HUD::get_HUD()
		{
			return m_hud;
		}
		UIElement HUD::get_timerrect()
		{
			return m_timerrect;
		}
	}
}