//Player_Car.cpp
#include "stdafx.h"
#include <helium/gameplay/Entity/3D/Player_Car.h>
#include <iostream>
#include <helium/time_delta.h>

using namespace helium;

player_car::player_car()
{
	for (size_t i = 0; i < sizeof(keys); i++)
	{
		keys[i] = false;
	}
	
	m_up = { 0.f, 1.f, 0.f };
	m_forward = { 0.f, 0.f, 1.f };
	m_right = { 1.f, 0.f, 0.f };

	acceleration = { 0.f, 0.f, 0.f };
	velocity = { 0.f, 0.f, 0.0f };
	velocity0 = { 0.f, 0.f, 0.0f };
	position = { 0.f, 0.f, 0.f };
	front = { 1.f, 0.f, 0.f };
	friction = {};
	chann1 = nullptr;
	chann2 = nullptr;
	m_sound_player = nullptr;
	m_sound_effect_acc = nullptr;
	m_sound_effect_brake = nullptr;
}
player_car::~player_car()
		{
			ServiceLocator<OSEventDispatcher>::get_service()->detach(this);
		}
void player_car::update()
{
	const float speed = .1f;
	DT = DeltaTime::GetDeltaTime();
	m_audio_system->stop(m_sound_effect_acc);
	m_audio_system->stop(m_sound_effect_brake);
	velocity0 = velocity;
	Vector3 right(1.0f, 0.0f, 0.0f);
	Vector3 up(0.0f, 1.0f, 0.0f);
	Vector3 forward(0.0f, 0.0f, 1.0f);

	Matrix4 ry = Matrix4::rotation(up, yaw);
	right = ry * right;
	forward = ry * forward;

	Matrix4 rx = Matrix4::rotation(right, pitch);
	up = rx * up;
	forward = rx * forward;

	right.normalize();
	up.normalize();
	forward.normalize();

	float fCoefficient = 0.5f;

	Vector3 frictonN = up * -1.f * mass;
	friction = velocity * -1.f * fCoefficient;



	m_right = right;
	m_up = up;
	m_forward = forward;

	m_carBody.local = m_carBody.local.rotation(m_up, yaw);
	front = Vector3(m_carBody.local.m._11, m_carBody.local.m._12, m_carBody.local.m._13);

	handle_input();
	velocity = velocity0 + (acceleration * (DT + speed));
	position += velocity * 0.5f;
	m_carBody.local = m_carBody.local.translation(m_carBody.local, position);
	velocity *= 0.5f;
	/*if (acceleration.length_squared() > 1.f)
	{
		acceleration.normalize();
	}*/
	check_bounds();

}
void player_car::check_bounds()
{
	if (position.m_x < -0.0f || position.m_x > 200.0f)
	{
		std::cout << "X: " << position.m_x << std::endl;
		set_position();
	}
	else if (position.m_z > 1.0f || position.m_z < -200.0f)
	{
		std::cout << "Z: " << position.m_z << std::endl;
		set_position();
	}
}
void player_car::handle_input()
{
	//arrow up
	if (keys[0])
	{
		m_audio_system->play(m_sound_effect_acc);
		acceleration += front * DT;
	}
	//arrow down
	if (keys[1])
	{
		m_audio_system->play(m_sound_effect_brake);
		acceleration -= front * DT;
	}
	//arrow right
	if (keys[2])
	{
		yaw -= 0.005f;
		if (yaw < -2.f * Math::PI)
		{
			yaw = 0;
		}
		velocity += front * DT;
	}//Jag vill g�ra fyra checkpoint rutor som skall kollideras mot i ordning. j�mf�r player_cars x och z position och ha rutorna gjorda i samma variabler sen kolla bara.  
	//arrow left
	if (keys[3])
	{
		yaw += 0.005f;
		if (yaw > 2.f * Math::PI)
		{
			yaw = 0.f;
		}
		velocity += front * DT;
	}
	//space
	if (keys[4])
	{
		acceleration = Vector3(0.f, 0.f, 0.f);
		velocity -= velocity*0.0001f;
	}
}

void player_car::initialize()
{
	ServiceLocator<OSEventDispatcher>::get_service()->attach(this);
	m_audio_system = ServiceLocator<system::AudioSystem>::get_service();
	m_sound_effect_acc = m_audio_system->create_sound("../data/music/Car_Accelerating.mp3", chann1, true, true);
	m_sound_effect_brake = m_audio_system->create_sound("../data/music/break.mp3", chann2, true, true);
	m_sound_player = m_audio_system->get_FMOD_system();
}

void player_car::notify(OSEvent* event)
{
	if (event->m_type == EOSEventType::Keyboard)
	{
		bool state = event->m_data.keyboard.state;

		if (event->m_data.keyboard.keycode == VK_UP)
		{
			keys[0] = state;
		}
		if (event->m_data.keyboard.keycode == VK_DOWN)
		{
			keys[1] = state;
		}
		if (event->m_data.keyboard.keycode == VK_RIGHT)
		{
			keys[2] = state;
		}
		if (event->m_data.keyboard.keycode == VK_LEFT)
		{
			keys[3] = state;
		}
		if (event->m_data.keyboard.keycode == VK_SPACE)
		{
			keys[4] = state;
		}
	}
}

void player_car::draw_car(sun::Lighting p_sun, scene::Camera& camera)
{
	m_carBody.m_render_system->select_shader(m_carBody.m_shader);
	m_carBody.m_render_system->select_texture(m_carBody.m_texture);
	m_carBody.m_render_system->select_sampler_state(m_carBody.m_sampler);
	m_carBody.m_render_system->select_vertex_format(m_carBody.m_format);
	m_carBody.m_render_system->select_vertex_buffer(m_carBody.m_buffer);
	m_carBody.m_render_system->select_depth_state(m_carBody.m_depth);
	m_carBody.m_render_system->select_rasterizer_state(m_carBody.m_raster);
	m_carBody.m_render_system->set_shader_constant_matrix4("projection", camera.get_projection());
	m_carBody.m_render_system->set_shader_constant_matrix4("view", camera.get_view());
	m_carBody.m_render_system->set_shader_constant_matrix4("world", m_carBody.local);
	m_carBody.m_render_system->set_shader_constant_vector3("direction", p_sun.direction);
	m_carBody.m_render_system->set_shader_constant_vector3("diffuse", p_sun.diffuse);
	m_carBody.m_render_system->set_shader_constant_vector3("ambient", p_sun.ambient);
	m_carBody.m_render_system->set_shader_constant_vector3("eye", camera.get_position());
	m_carBody.m_render_system->set_shader_constant_raw("material", &m_carBody.material, sizeof(m_carBody.material));
	m_carBody.m_render_system->apply();
	m_carBody.m_render_system->draw(system::EDrawMode::TriangleList, 0, 36);//drawcount
}

Vector3 player_car::get_position()
{
	return position;
}
void player_car::set_position() //Ladbon
{
	position = { 0.0f, 0.0f, 0.0f };
	acceleration = { 0.0f, 0.0f, 0.0f };
	velocity = { 0.0f, 0.0f, 0.0f };
	velocity0 = { 0.0f, 0.0f, 0.0f };
	m_carBody.local.identity();
}
Matrix4 player_car::get_translation()
{
	return m_carBody.local;
}