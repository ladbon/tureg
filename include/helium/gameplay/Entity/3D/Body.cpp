#include "stdafx.h"
#include <helium/gameplay/entity/3d/body.h>

void helium::Body::initialize()
{
	m_render_system = ServiceLocator<helium::system::RenderSystem>::get_service();
}

void helium::Body::create_body()
{
	resource::Shader::Ptr shader = resource::Shader::create("../data/example.shader.txt");
	m_shader = m_render_system->create_shader(shader->get_vertex_source().c_str(),
		shader->get_pixel_source().c_str());

	resource::VertexFormat::Ptr format = resource::VertexFormat::create(
		"../data/example.vertexformat.txt");
	m_format = m_render_system->create_vertex_format(
		const_cast<system::VertexFormatDesc*>(format->get_desc()),
		format->get_count(),
		m_shader);//

	resource::Texture::Ptr texture = resource::Texture::create(
		"../data/crate.png");
	m_texture = m_render_system->create_texture(
		texture->get_format(),
		texture->get_type(),
		texture->get_width(),
		texture->get_height(),
		texture->get_depth(),
		texture->get_pixel_data());

	resource::Sampler::Ptr sampler = resource::Sampler::create(
		"../data/example.sampler.txt");
	m_sampler = m_render_system->create_sampler_state(
		sampler->get_filter_mode(),
		sampler->get_addr_u(),
		sampler->get_addr_v(),
		sampler->get_addr_w());

	m_blend = m_render_system->create_blend_state(
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendOp::Add, system::EBlendOp::Add);

	material.ambient = 0.1f;
	material.diffuse = 0.5f;
	material.specular = 0.5f;
	material.shininess = 32.0f;

	m_raster = m_render_system->create_rasterizer_state(
		system::EFillMode::Solid,
		system::ECullMode::Back);

	m_depth = m_render_system->create_depth_state(
		false, false, system::EDepthMode::Less);


	struct Vertex
	{
		Vector3 pos;
		Vector2 texpos;
		Vector3 normal;
	};
	float p = 1.0f;
	Vertex vertices[] =
	{
		//front
		{ Vector3(-p, p, -p), Vector2(0.f, 0.f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(p, p, -p), Vector2(1.f, 0.f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(p, -p, -p), Vector2(1.f, 1.f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(p, -p, -p), Vector2(1.f, 1.f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(-p, -p, -p), Vector2(0.f, 1.f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(-p, p, -p), Vector2(0.f, 0.f), Vector3(0.0f, 0.0f, -1.0f) },

		//right
		{ Vector3(p, p, -p), Vector2(0.f, 0.f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(p, p, p), Vector2(1.f, 0.f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(p, -p, p), Vector2(1.f, 1.f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(p, -p, p), Vector2(1.f, 1.f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(p, -p, -p), Vector2(0.f, 1.f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(p, p, -p), Vector2(0.f, 0.f), Vector3(1.0f, 0.0f, 0.0f) },

		//back
		{ Vector3(p, p, p), Vector2(0.f, 0.f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(-p, p, p), Vector2(1.f, 0.f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(-p, -p, p), Vector2(1.f, 1.f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(-p, -p, p), Vector2(1.f, 1.f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(p, -p, p), Vector2(0.f, 1.f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(p, p, p), Vector2(0.f, 0.f), Vector3(0.0f, 0.0f, 1.0f) },

		//left
		{ Vector3(-p, p, p), Vector2(0.f, 0.f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-p, p, -p), Vector2(1.f, 0.f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-p, -p, -p), Vector2(1.f, 1.f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-p, -p, -p), Vector2(1.f, 1.f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-p, -p, p), Vector2(0.f, 1.f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-p, p, p), Vector2(0.f, 0.f), Vector3(-1.0f, 0.0f, 0.0f) },

		//top
		{ Vector3(-p, p, p), Vector2(0.f, 0.f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(p, p, p), Vector2(1.f, 0.f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(p, p, -p), Vector2(1.f, 1.f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(p, p, -p), Vector2(1.f, 1.f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(-p, p, -p), Vector2(0.f, 1.f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(-p, p, p), Vector2(0.f, 0.f), Vector3(0.0f, 1.0f, 0.0f) },

		//bottom
		{ Vector3(-p, -p, -p), Vector2(0.f, 0.f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(p, -p, -p), Vector2(1.f, 0.f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(p, -p, p), Vector2(1.f, 1.f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(p, -p, p), Vector2(1.f, 1.f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(-p, -p, p), Vector2(0.f, 1.f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(-p, -p, -p), Vector2(0.f, 0.f), Vector3(0.0f, -1.0f, 0.0f) },
	};

	m_buffer = m_render_system->create_vertex_buffer(
		system::EBufferMode::Static,
		vertices,
		sizeof(Vertex),
		sizeof(vertices) / sizeof(vertices[0]));

}