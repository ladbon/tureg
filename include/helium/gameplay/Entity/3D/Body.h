//Body.h

//Note(orre): this is a body for the car(s)

#ifndef BODY_H
#define BODY_H

#include <helium/math.hpp>
#include <helium/system/render_system.hpp>
#include <helium/service_locator.hpp>
#include <helium/resource/vertex_format.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/shader.hpp>

namespace helium
{
	class Body
	{
	public:
		Body(){}
		~Body(){}

		void initialize();
		void create_body();
		
		
		helium::system::RenderSystem* m_render_system;
		struct Material
		{
			float ambient;
			float diffuse;
			float specular;
			float shininess;
		};


		int m_shader;
		int m_texture;
		int m_sampler;
		int m_format;
		int m_buffer;
		int m_index;
		int m_blend;
		int m_depth;
		int m_raster;
		int m_drawcount;
		Material material;
		Matrix4 local;
	};
}
#endif