//Base_Car.h

//Note(orre) this is the base class for the game racing agents, the car and possibly opponents

#ifndef BASE_CAR_HPP_INCLUDED
#define BASE_CAR_HPP_INCLUDED

#include <stdafx.h>
#include <helium/os_event.hpp>
#include <helium/service_locator.hpp>
#include <helium/os_event_dispatcher.hpp>

namespace helium
{
	class base_car : public OSEventListener
	{
	public:
		base_car()
		{}
		~base_car()
		{}
		//todo(orre) see if I should have this pointer in the base class, or have 
		//pointers to the specific implementations, or not have such poiters at all
		virtual void helium::OSEventListener::notify(OSEvent*)
		{}

		typedef std::unique_ptr<base_car> Ptr;
		static Ptr create()
		{
			return base_car::Ptr(new base_car);
		}
		void init()
		{
			ServiceLocator<OSEventDispatcher>::get_service()->attach(this);
		}



		virtual void update(){}
		virtual void handle_input() {}
	protected:
		bool keys[5];

		float yaw = 0.f;
		float pitch = 0.f;

		unsigned int mass = 3;

		Vector3 m_right;
		Vector3 m_up;
		Vector3 m_forward;
		Vector3 acceleration;
		Vector3 velocity;
		Vector3 velocity0;
		Vector3 position;
		Vector3 front;
		Vector3 friction;


	};

}

#endif