//Player_Car.h
//note(orre) this is the car that the player is controlling
#pragma once

#ifndef PLAYER_CAR_HPP_INCLUDED
#define PLAYER_CAR_HPP_INCLUDED

#include "helium/gameplay/entity/3D/Base_car.h"
#include <helium/gameplay/entity/3D/Body.h>
#include <helium/gameplay/surroundings/sun.h>
#include <helium/scene/scene.hpp>
#include <helium/system/audio_system.hpp>

#include <fmod.hpp>
#include <fmod_errors.h>
#pragma comment(lib, "fmod_vc.lib")
#pragma comment(lib, "fmodL_vc.lib")

namespace helium
{

	class player_car : public base_car
	{
	public:
		player_car();
		~player_car();

		void update();
		void initialize();
		void handle_input();
		void notify(OSEvent* evento);
		void draw_car(sun::Lighting p_sun, scene::Camera& camera);
		Vector3 get_position();
		void set_position();
		void check_bounds();

		Matrix4 get_translation();
		Body m_carBody;
		system::AudioSystem* m_audio_system;
		helium::system::AudioSystem::Sound* m_sound_effect_acc;
		helium::system::AudioSystem::Sound* m_sound_effect_brake;
		FMOD::Sound* sound;
		FMOD::Channel* chann1;
		FMOD::Channel* chann2;
		FMOD::System* m_sound_player;
		
		float DT;
		float throttle;
	};
	
}
#endif
