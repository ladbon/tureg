//Checkpoint.cpp

#include <stdafx.h>
#include <helium/gameplay/entity/gamerules/CheckpointManager.h>
#include <iostream>

namespace helium
{
	// static 
	CheckpointManager::Ptr CheckpointManager::create()
	{
		return CheckpointManager::Ptr(new CheckpointManager);
	}
	CheckpointManager::CheckpointManager()
	{
		AddCheckpoint(Vector4(5.0f, 30.0f, 0.0f, -30.0f));
		AddCheckpoint(Vector4(170.0f, 200.0f, 0.0f, -30.0f));
		AddCheckpoint(Vector4(180.0f, 200.0f, -170.0f, -200.0f));
		AddCheckpoint(Vector4(30.0f, 0.0f, -170.0f, -200.0f));

	}
	CheckpointManager::~CheckpointManager()
	{

	}
	bool CheckpointManager::update(Vector3 p_position)
	{
		// Checkpoint Juan
		if (m_checkpoints.at(0).s_check == false)
		{
			if (p_position.m_x >= m_checkpoints.at(0).s_square.m_x &&
				p_position.m_x <= m_checkpoints.at(0).s_square.m_y &&
				p_position.m_z <= m_checkpoints.at(0).s_square.m_z &&
				p_position.m_z >= m_checkpoints.at(0).s_square.m_w)
			{
				std::cout << "CHECKPOINT 1" << std::endl;
				m_checkpoints.at(0).s_check = true;
				return false;
			}
			return false;
		}
		// Checkpoint Dos
		else if (m_checkpoints.at(1).s_check == false)
		{
			if (p_position.m_x >= m_checkpoints.at(1).s_square.m_x &&
				p_position.m_x <= m_checkpoints.at(1).s_square.m_y &&
				p_position.m_z <= m_checkpoints.at(1).s_square.m_z &&
				p_position.m_z >= m_checkpoints.at(1).s_square.m_w)
			{

				std::cout << "CHECKPOINT 2" << std::endl;
				m_checkpoints.at(1).s_check = true;
				return false;
			}
			return false;
		}
		// Checkpoint Tres
		else if (m_checkpoints.at(2).s_check == false)
		{
			if (p_position.m_x >= m_checkpoints.at(2).s_square.m_x &&
				p_position.m_x <= m_checkpoints.at(2).s_square.m_y &&
				p_position.m_z <= m_checkpoints.at(2).s_square.m_z &&
				p_position.m_z >= m_checkpoints.at(2).s_square.m_w)
			{
				std::cout << "CHECKPOINT 3" << std::endl;
				m_checkpoints.at(2).s_check = true;
				return false;
			}
			return false;
		}
		// Checkpoint Quatro
		else if (m_checkpoints.at(3).s_check == false)
		{
			if (p_position.m_x <= m_checkpoints.at(3).s_square.m_x &&
				p_position.m_x >= m_checkpoints.at(3).s_square.m_y &&
				p_position.m_z <= m_checkpoints.at(3).s_square.m_z &&
				p_position.m_z >= m_checkpoints.at(3).s_square.m_w)
			{

				std::cout << "CHECKPOINT 4" << std::endl;
				m_checkpoints.at(3).s_check = true;
				return false;
			}
		}
		// Check if won
		else if (m_checkpoints.at(0).s_check == true &&
			m_checkpoints.at(1).s_check == true &&
			m_checkpoints.at(2).s_check == true &&
			m_checkpoints.at(3).s_check == true)
		{
			std::cout << "GAMEOVER" << std::endl;
			return true;
		}
		// fail-safe
		else
		{
			return false;
		}
		// fail-safe
		return false;
	}
	void CheckpointManager::AddCheckpoint(Vector4 p_checkpoint)
	{
		Checkpoint xcheckpoint = { false, p_checkpoint };
		m_checkpoints.push_back(xcheckpoint);
	}
}