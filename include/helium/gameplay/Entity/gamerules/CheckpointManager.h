//CheckpointManager.h

#ifndef CHECKPOINTMANAGER_H_INCLUDED
#define CHECKPOINTMANAGER_H_INCLUDED

#include <helium/math.hpp>
#include <vector>

namespace helium
{
	struct Checkpoint
	{
		bool s_check;
		Vector4 s_square;
	};

	class CheckpointManager
	{
	public:

		typedef std::unique_ptr<CheckpointManager> Ptr;

		static Ptr create();

		CheckpointManager();
		~CheckpointManager();
		bool update(Vector3 p_position);
		void AddCheckpoint(Vector4 p_checkpoint);

	private:
		std::vector<Checkpoint>m_checkpoints;
		
	};

}

#endif //CheckpointManager.h
