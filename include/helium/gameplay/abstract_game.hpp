// abstract_game.hpp

#ifndef ABSTRACTGAME_HPP_INCLUDED
#define ABSTRACTGAME_HPP_INCLUDED

#include <helium/os_event_listener.hpp>
#include <helium/gameplay/state_manager.hpp>

namespace helium
{
	namespace gameplay
	{
		// note(tommi): inherit from this class and implement your specific game
		//   there are more 'systems' to add e.g network, physics, ...
		class AbstractGame 
		{
		public:
			typedef std::unique_ptr<AbstractGame> Ptr;
			
			static Ptr create();

		public:
			virtual ~AbstractGame();
			
			virtual void initialize();
			virtual void shutdown();
			virtual void process();

		protected:
			StateManager::Ptr m_state_manager;
			system::CollisionSystem* m_collision_system;
			system::RenderSystem* m_render_system;
			system::AudioSystem* m_audio_system;
		};
	}
}

#endif // ABSTRACTGAME_HPP_INCLUDED
