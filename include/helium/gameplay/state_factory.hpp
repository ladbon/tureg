// state_factory.hpp

#ifndef STATEFACTORY_HPP_INCLUDED
#define STATEFACTORY_HPP_INCLUDED

namespace helium
{
	namespace gameplay
	{
		class AbstractState;

		class StateFactory
		{
			class AbstractCreator
			{
			public:
				virtual ~AbstractCreator() {};
				virtual AbstractState* create() = 0;
			};

			template <class T>
			class Creator : public AbstractCreator
			{
			public:
				Creator() {};
				AbstractState* create() { return new T; }
			};

		private: // non-copyable
			StateFactory(const StateFactory&);
			StateFactory& operator=(const StateFactory&);

		public:
			typedef std::unique_ptr<StateFactory> Ptr;

			static Ptr create();

		private:
			StateFactory();

		public:
			~StateFactory();

			template <class T>
			void attach(uint32_t id_hash)
			{
				auto it = m_creators.find(id_hash);
				if (it != m_creators.end())
				{
					Debug::write(EDebugLevel::Warning, "statefactory: override previous state");
					delete it->second;
				}
				m_creators[id_hash] = new Creator<T>();
			}

			AbstractState* construct(uint32_t id_hash);

		private:
			std::unordered_map<uint32_t, AbstractCreator*> m_creators;
		};
	}
}

#endif // STATEFACTORY_HPP_INCLUDED
