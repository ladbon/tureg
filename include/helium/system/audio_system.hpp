// audio_system.hpp

#ifndef AUDIOSYSTEM_HPP_INCLUDED
#define AUDIOSYSTEM_HPP_INCLUDED

namespace FMOD
{
	class Sound;
	class Channel;
	class ChannelGroup;
	class System;
}

namespace helium
{
	namespace system
	{
		enum class EAudioType : uint32_t
		{
			Buffer,
			Stream,
			Invalid,
		};

		class AudioEmitter
		{
		private:
			friend class AudioSystem;
			AudioEmitter(uint32_t id);

		private:
			uint32_t m_id;
			Vector3 m_position;
			Vector3 m_velocity;
		};

		class AudioListener
		{
		private:
			friend class AudioSystem;
			AudioListener(uint32_t id);

		private:
			uint32_t m_id;
			Vector3 m_position;
			Vector3 m_velocity;
		};

		class AudioSystem
		{
		public:
			struct Sound;

		private: // non-copyable
			AudioSystem(const AudioSystem&);
			AudioSystem& operator=(const AudioSystem&);

		public:
			typedef std::unique_ptr<AudioSystem> Ptr;

			static Ptr create();

			Sound* create_sound(const char*, FMOD::Channel*, bool pause, bool loop);
			Sound* create_music(const char*, FMOD::Channel*, bool pause, bool loop);
			FMOD::ChannelGroup* get_channelgroup_effect();
			FMOD::Channel** get_channel_effect();
			FMOD::Sound* get_sound_effect();
			void play(Sound*);
			void stop(Sound*);
			FMOD::System* get_FMOD_system();
		private:
			AudioSystem();

		public:
			~AudioSystem();

			void process();

		private:
			int  add_sound(Sound* sound);

		private:
			FMOD::System* m_system;
			FMOD::ChannelGroup* m_master;
			FMOD::ChannelGroup* m_music;
			FMOD::ChannelGroup* m_effect;

		private:
			std::vector<Sound*> m_sounds;
		};
	}
}

#endif // AUDIOSYSTEM_HPP_INCLUDED
