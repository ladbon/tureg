// service_locator.hpp

#ifndef SERVICELOCATOR_HPP_INCLUDED
#define SERVICELOCATOR_HPP_INCLUDED

namespace helium
{
	template <class Service>
	class ServiceLocator
	{
	private:
		ServiceLocator(const ServiceLocator<Service>&);
		ServiceLocator& operator=(const ServiceLocator<Service>&);

	public:
		ServiceLocator() {}

		static void set_service(Service* service)
		{
			ms_service = service;
		}

		static Service* get_service()
		{
			return ms_service;
		}

	private:
		static Service* ms_service;
	};

	template <class Service> 
	Service* ServiceLocator<Service>::ms_service = nullptr;
}

#endif // SERVICELOCATOR_HPP_INCLUDED
