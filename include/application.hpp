// application.hpp

#ifndef APPLICATION_HPP_INCLUDED
#define APPLICATION_HPP_INCLUDED

#include <helium/os_event_listener.hpp>
#include <helium/os_event_dispatcher.hpp>
#include <helium/system/audio_system.hpp>
#include <helium/system/collision_system.hpp>
#include <helium/system/render_system.hpp>
#include <helium/resource/resource_cache.hpp>
#include <helium/gameplay/abstract_game.hpp>
#include <helium/time_delta.h>

using namespace helium;

class Application : public OSEventListener
{
public:
	Application();
	~Application();

	bool initialize(HWND window_handle, int width, int height);
	void shutdown();
	bool process();

	system::RenderSystem* getRenderPtr();
private:
	void notify(OSEvent* event);

private:
	bool m_running;
	OSEventDispatcher::Ptr m_event_dispatcher;
	system::RenderSystem::Ptr m_render_system;
	system::AudioSystem::Ptr m_audio_system;
	system::CollisionSystem::Ptr m_collision_system;
	resource::ResourceCache::Ptr m_resource_cache;
	gameplay::AbstractGame::Ptr m_game;

};

#endif // APPLICATION_HPP_INCLUDED
