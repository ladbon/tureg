// mouse.cpp

#include "stdafx.h"
#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>
#include <helium/service_locator.hpp>
#include <helium/mouse.hpp>

namespace helium
{
	//static 
	Mouse::Ptr Mouse::create()
	{
		return Mouse::Ptr(new Mouse);
	}

	// private
	Mouse::Mouse()
	{
		m_x = 0;
		m_y = 0;
		m_delta = 0.0f;
		memset(m_curr, 0, sizeof(m_curr));
		memset(m_prev, 0, sizeof(m_prev));
		ServiceLocator<OSEventDispatcher>::get_service()->attach(this);
	}

	// public
	Mouse::~Mouse()
	{
		ServiceLocator<OSEventDispatcher>::get_service()->detach(this);
	}

	bool Mouse::is_button_down(EMouseButton btn) const
	{
		return m_curr[(uint32_t)btn];
	}

	bool Mouse::is_button_down_once(EMouseButton btn) const
	{
		return m_curr[(uint32_t)btn] && !m_prev[(uint32_t)btn];
	}

	int32_t Mouse::get_x() const
	{
		return m_x;
	}

	int32_t Mouse::get_y() const
	{
		return m_y;
	}

	void Mouse::post_frame()
	{ 
		m_delta = 0.0f;
		memcpy(m_prev, m_curr, sizeof(m_prev));
	}

	// private
	void Mouse::notify(OSEvent* event)
	{
		if (event->m_type == EOSEventType::MouseMove)
		{
			m_x = event->m_data.mouse.move.x;
			m_y = event->m_data.mouse.move.y;
		}
		else if (event->m_type == EOSEventType::MouseButton)
			m_curr[(uint32_t)event->m_data.mouse.button.button] = event->m_data.mouse.button.state;
		else if (event->m_type == EOSEventType::MouseWheel)
			m_delta = event->m_data.mouse.wheel.delta;
	}
}
