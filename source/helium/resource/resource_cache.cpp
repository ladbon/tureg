// resource_cache.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>
#include <helium/system/render_system.hpp>
#include <helium/resource/resource_cache.hpp>

namespace helium
{
	namespace resource
	{
		// static 
		ResourceCache::Ptr ResourceCache::create()
		{
			return ResourceCache::Ptr(new ResourceCache);
		}

		// private
		ResourceCache::ResourceCache()
		{
			m_audio_system = ServiceLocator<system::AudioSystem>::get_service();
			m_render_system = ServiceLocator<system::RenderSystem>::get_service();
		}

		// public
		ResourceCache::~ResourceCache()
		{
		}

		void ResourceCache::set_directory(const std::string& directory)
		{
			m_directory = directory;
		}

		bool ResourceCache::has_resource(const std::string& filename, const std::string& group)
		{
			uint32_t group_hash = String::hash32(group.c_str(), (uint32_t)group.length());
			uint32_t name_hash = String::hash32(filename.c_str(), (uint32_t)filename.length());
			auto it = m_groups.find(group_hash);
			if (it == m_groups.end())
			{
				Debug::write(EDebugLevel::Warning, 
					String::format("ResourceCache: trying to access group that doesn't exist: %s", 
					group.c_str()));
				return false;
			}

			return it->second.m_resources.find(name_hash) != it->second.m_resources.end();
		}

		void ResourceCache::unload_resource(const std::string& filename, const std::string& group)
		{
			uint32_t group_hash = String::hash32(group.c_str(), (uint32_t)group.length());
			uint32_t name_hash = String::hash32(filename.c_str(), (uint32_t)filename.length());
			auto it = m_groups.find(group_hash);
			if (it == m_groups.end())
			{
				Debug::write(EDebugLevel::Warning, 
					String::format("resourcecache: trying to access group that doesn't exist: %s", 
					group.c_str()));
				return;
			}

			auto res = it->second.m_resources.find(name_hash);
			if (res == it->second.m_resources.end())
			{
				Debug::write(EDebugLevel::Warning, 
					String::format("resourcecache: trying to access resource that doesn't exist: %s [%s]", 
					filename.c_str(), group.c_str()));
				return;
			}

			res->second->release();
			it->second.m_resources.erase(res);

			Debug::write(EDebugLevel::Info, 
				String::format("resourcecache: releasing resource: %s [%s]", 
				filename.c_str(), group.c_str()));
		}

		void ResourceCache::unload_all_resources(const std::string& group)
		{
			uint32_t group_hash = String::hash32(group.c_str(), (uint32_t)group.length());
			auto it = m_groups.find(group_hash);
			if (it == m_groups.end())
			{
				Debug::write(EDebugLevel::Warning, 
					String::format("resourcecache: trying to access group that doesn't exist: %s", 
					group.c_str()));
				return;
			}

			Debug::write(EDebugLevel::Warning, 
				String::format("resourcecache: releasing whole group: [%s]", 
				group.c_str()));
			auto res = it->second.m_resources.begin();
			while (res != it->second.m_resources.end())
			{
				res->second->release();

				/*Debug::write(EDebugLevel::Info, 
					String::format("  resource: %ULL", 
					res->first));*/
				++res;
			}
			it->second.m_resources.clear();
		}
	}
}
