// font.cpp

#include "stdafx.h"
#include <helium/resource/font.hpp>

namespace helium
{
	namespace resource
	{
		//static 
		Font::Ptr Font::create(const std::string& filename)
		{
			std::ifstream stream;
			stream.open(filename);
			if (!stream.is_open())
			{
				Debug::write(EDebugLevel::Error,
					String::format("font: could not find file %s",
					filename.c_str()));
				return Font::Ptr(nullptr);
			}

			std::unordered_map<uint32_t, Glyph> glyphs;
			uint32_t count = 0;
			stream >> count;
			for (uint32_t i = 0; i < count; i++)
			{
				uint32_t c = 0;
				Glyph g;

				stream >> c;
				stream >> g.width;
				stream >> g.height;
				stream >> g.texcoord.m_x;
				stream >> g.texcoord.m_y;
				stream >> g.texcoord.m_z;
				stream >> g.texcoord.m_w;

				glyphs.insert(std::pair<uint32_t, Glyph>(c, g));
			}
			stream.close();

			Font* font = new Font;
			font->m_glyphs = glyphs;
			return Font::Ptr(font);
		}

		// private
		Font::Font()
		{
		}

		// public
		Font::~Font()
		{
		}

		bool Font::has_glyph(uint32_t unicode) const
		{
			return m_glyphs.find(unicode) != m_glyphs.end();
		}

		const Glyph& Font::get_glyph(uint32_t unicode) const
		{
			return m_glyphs.find(unicode)->second;
		}

		bool Font::has_texture() const
		{
			return m_texture != nullptr;
		}

		void Font::set_texture(Texture* texture)
		{
			m_texture = texture;
		}

		const Texture* Font::get_texture() const
		{
			return m_texture;
		}
	}
}
