// abstract_game.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>
#include <helium/system/collision_system.hpp>
#include <helium/system/render_system.hpp>
#include <helium/gameplay/abstract_game.hpp>

namespace helium
{
	namespace gameplay
	{
		using namespace helium::system;

		//static 
		AbstractGame::Ptr AbstractGame::create()
		{
			return AbstractGame::Ptr(new AbstractGame);
		}

		// public
		AbstractGame::~AbstractGame()
		{
		}

		void AbstractGame::initialize()
		{
			m_state_manager = StateManager::create();
			m_state_manager->set_factory(StateFactory::create());
			m_collision_system = ServiceLocator<system::CollisionSystem>::get_service();
			m_render_system = ServiceLocator<system::RenderSystem>::get_service();
			m_audio_system = ServiceLocator<system::AudioSystem>::get_service();
		}

		void AbstractGame::shutdown()
		{
			m_state_manager->set_state(0);
		}

		void AbstractGame::process()
		{
			//m_audio_system->process();
			m_state_manager->update();
			m_collision_system->process();
			m_render_system->clear();
			m_state_manager->draw();
			m_render_system->present();
		}
	}
}