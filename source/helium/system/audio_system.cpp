// audio_system.cpp

#include "stdafx.h"
#include <fmod.hpp>
#include <fmod_errors.h>
#pragma comment(lib, "fmod_vc.lib")
#pragma comment(lib, "fmodL_vc.lib")
#include <helium/system/audio_system.hpp>

namespace helium
{
	namespace system
	{
		// struct
		struct AudioSystem::Sound
		{
			FMOD::Sound* m_sound;
			FMOD::Channel* m_channel;
			EAudioType m_type;
		};

		// emitter
		AudioEmitter::AudioEmitter(uint32_t id)
		{
			m_id = id;
		}

		// listener
		AudioListener::AudioListener(uint32_t id)
		{
			m_id = id;
		}

		// static 
		AudioSystem::Ptr AudioSystem::create()
		{
			return AudioSystem::Ptr(new AudioSystem);
		}

		// private
		AudioSystem::AudioSystem()
		{
			FMOD_RESULT result = FMOD_OK;

			result = FMOD::System_Create(&m_system);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not create system");

			result = m_system->init(32, FMOD_INIT_NORMAL, NULL);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not initialize system");
			Debug::write(EDebugLevel::Info, "audiosystem: initialize");

			result = m_system->createChannelGroup("music", &m_music);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not create channel group music");
			Debug::write(EDebugLevel::Info, "  created channel group: music");
			result = m_music->setVolume(0.5f);

			result = m_system->createChannelGroup("effect", &m_effect);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not create channel group effect");
			Debug::write(EDebugLevel::Info, "  created channel group: effect");
			result = m_effect->setVolume(0.5f);

			result = m_system->getMasterChannelGroup(&m_master);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not obtain channel group master");
			Debug::write(EDebugLevel::Info, "  get channel group: master");
			result = m_master->addGroup(m_music);
			result = m_master->addGroup(m_effect);
		}

		// public
		AudioSystem::~AudioSystem()
		{
			for (unsigned i = 0; i < m_sounds.size(); i++)
			{
				m_sounds[i]->m_sound->release();
				if (m_sounds[i])
				{
					delete m_sounds[i];
				}
			}
			m_sounds.clear();

			m_music->release();
			m_music = nullptr;

			m_effect->release();
			m_effect = nullptr;

			m_master->release();
			m_master = nullptr;

			m_system->release();
			m_system = nullptr;

			Debug::write(EDebugLevel::Info, "audiosystem: shutdown");
		}

		void AudioSystem::process()
		{
			for (unsigned i = 0; i < m_sounds.size(); i++)
			{
				if (m_sounds[i]->m_channel)
				{
					bool playing = false;
					m_sounds[i]->m_channel->isPlaying(&playing);
					if (!playing)
						m_sounds[i]->m_channel = nullptr;
				}
			}

			m_system->update();
		}

		// private
		int AudioSystem::add_sound(Sound* sound)
		{
			unsigned index = (uint32_t)-1;
			for (unsigned i = 0; i < m_sounds.size(); i++)
			{
				if (!m_sounds[i]->m_sound)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				m_sounds[index] = sound;
				return index;
			}

			m_sounds.push_back(sound);
			return (int)m_sounds.size() - 1;
		}
		AudioSystem::Sound* AudioSystem::create_music(const char* filename, FMOD::Channel* chan, bool pause, bool loop)
		{
			Sound* sound = new Sound;
			sound->m_channel = chan;
			sound->m_type = EAudioType::Buffer;
			m_system->createSound(filename, FMOD_DEFAULT, 0, &sound->m_sound);
			m_system->playSound(sound->m_sound, m_music, pause, &sound->m_channel);
			sound->m_channel->setChannelGroup(m_music);
			if (loop)
			{
				sound->m_channel->setMode(FMOD_LOOP_NORMAL);
			}
			add_sound(sound);
			return sound;
		}
		AudioSystem::Sound* AudioSystem::create_sound(const char* filename, FMOD::Channel* chan, bool pause, bool loop)
		{
			Sound* sound = new Sound;
			sound->m_channel = chan;
			sound->m_type = EAudioType::Buffer;
			m_system->createSound(filename, FMOD_DEFAULT, 0, &sound->m_sound);
			m_system->playSound(sound->m_sound, m_effect, pause, &sound->m_channel);
			sound->m_channel->setChannelGroup(m_effect);
			if (loop)
			{
				sound->m_channel->setMode(FMOD_LOOP_NORMAL);
			}
			add_sound(sound);
			return sound;
		}
		void AudioSystem::play(Sound* p_sound)
		{
			p_sound->m_channel->setPaused(false);
		}

		void AudioSystem::stop(Sound* p_sound)
		{
			p_sound->m_channel->setPaused(true);
		}

		FMOD::System* AudioSystem::get_FMOD_system()
		{
			return m_system;
		}
		FMOD::ChannelGroup* AudioSystem::get_channelgroup_effect()
		{
			return m_effect;
		}
		
	}
}
