// camera.cpp

#include "stdafx.h"
#include <helium/scene/camera.hpp>
#include <iostream>

namespace helium
{
	namespace scene
	{
		// static 
		Camera::Ptr Camera::create()
		{
			return Camera::Ptr(new Camera);
		}

		// private
		Camera::Camera()
			: m_right(1.0f, 0.0f, 0.0f)
			, m_up(0.0f, 1.0f, 0.0f)
			, m_forward(0.0f, 0.0f, 1.0f)
		{
			m_dirty = true;
			m_pitch = 0.0f;
			m_yaw = 0.0f;
			m_roll = 0.0f;
			m_view.identity();
			m_projection.identity();
		}

		// public
		Camera::~Camera()
		{
		}

		void Camera::update()
		{
			if (!m_dirty)
				return;

			m_dirty = false;

			Vector3 right(1.0f, 0.0f, 0.0f);
			Vector3 up(0.0f, 1.0f, 0.0f);
			Vector3 forward(0.0f, 0.0f, 1.0f);

			Matrix4 ry = Matrix4::rotation(up, m_yaw);
			right = ry * right;
			forward = ry * forward;

			Matrix4 rx = Matrix4::rotation(right, m_pitch);
			up = rx * up;
			forward = rx * forward;

			right.normalize();
			up.normalize();
			forward.normalize();

			m_right = right;
			m_up = up;
			m_forward = forward;

			// construct view matrix
			m_view.m._11 = m_right.m_x; m_view.m._12 = m_up.m_x; m_view.m._13 = m_forward.m_x; m_view.m._14 = 0.0f;
			m_view.m._21 = m_right.m_y; m_view.m._22 = m_up.m_y; m_view.m._23 = m_forward.m_y; m_view.m._24 = 0.0f;
			m_view.m._31 = m_right.m_z; m_view.m._32 = m_up.m_z; m_view.m._33 = m_forward.m_z; m_view.m._34 = 0.0f;
			m_view.m._41 = -m_position.dot(m_right);
			m_view.m._42 = -m_position.dot(m_up);
			m_view.m._43 = -m_position.dot(m_forward);
			m_view.m._44 = 1.0f;

			Matrix4 viewproj = m_view * m_projection;
			m_frustum.construct(viewproj);
		}

		void Camera::move_forward(float amount)
		{
			m_dirty = true;
			m_position += m_forward * amount;
		}

		void Camera::move_sidestep(float amount)
		{
			m_dirty = true;
			m_position += m_right * amount;
		}

		void Camera::move_elevate(float amount)
		{
			m_dirty = true;
			m_position += m_up * amount;
		}

		void Camera::rotatex(float amount)
		{
			m_dirty = true;
			m_pitch += amount;
		}

		void Camera::rotatey(float amount)
		{
			m_dirty = true;
			m_yaw += amount;
		}

		void Camera::rotatez(float amount)
		{
			m_dirty = true;
			m_roll += amount;
		}

		void Camera::set_perspective(float fov, float aspect, float znear, float zfar)
		{
			m_dirty = true;
			m_projection = Matrix4::perspective(fov, aspect, znear, zfar);
		}

		void Camera::set_orthogonal(float width, float height, float znear, float zfar)
		{
			m_dirty = true;
			m_projection = Matrix4::orthogonal(width, height, znear, zfar);
		}

		const Matrix4& Camera::get_view() const
		{
			return m_view;
		}

		const Matrix4& Camera::get_projection() const
		{
			return m_projection;
		}

		Vector3 Camera::get_position() const
		{
			return m_position;
		}

		void Camera::set_Position(Vector3 p_position)
		{
			m_position = p_position;
		}

		const Frustum& Camera::get_frustum() const
		{
			return m_frustum;
		}
	}
}
