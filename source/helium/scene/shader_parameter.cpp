// shader_parameter.cpp

#include "stdafx.h"
#include <helium/scene/shader_parameter.hpp>

namespace helium
{
	namespace scene
	{
		// static 
		ShaderParameter::Ptr ShaderParameter::create(const std::string& name)
		{
			return ShaderParameter::Ptr(new ShaderParameter(name));
		}

		// private
		ShaderParameter::ShaderParameter(const std::string& name)
#if !defined(NDEBUG)
			: m_name(name)
#endif
		{
			m_name_hash = String::hash32(name.c_str(), (uint32_t)name.length());
			m_size = 0;
			m_buffer = nullptr;
		}

		// public
		ShaderParameter::~ShaderParameter()
		{
		}

		bool ShaderParameter::is_valid() const
		{
			return !m_buffer && !m_size;
		}

		uint32_t ShaderParameter::get_name_hash() const
		{
			return m_name_hash;
		}

		uint32_t ShaderParameter::get_size() const
		{
			return m_size;
		}

		const void* ShaderParameter::get_buffer() const
		{
			return m_buffer;
		}

#if !defined(NDEBUG)
		const std::string& ShaderParameter::get_name() const
		{
			return m_name;
		}
#endif

		// private
		void ShaderParameter::set_size(uint32_t size)
		{
			m_size = size;
		}

		void ShaderParameter::set_buffer(const void* buffer)
		{
			m_buffer = const_cast<void*>(buffer);
		}
	}
}
