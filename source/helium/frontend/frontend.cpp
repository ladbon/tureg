// frontend.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/frontend/frontend.hpp>

namespace helium
{
	namespace frontend
	{
		// static 
		FrontEnd::Ptr FrontEnd::create()
		{
			return FrontEnd::Ptr(new FrontEnd);
		}

		// private
		FrontEnd::FrontEnd()
		{
			m_camera = nullptr;
			m_render_dispatcher = renderer::RenderDispatcher::create(
				ServiceLocator<system::RenderSystem>::get_service());
		}

		// public
		FrontEnd::~FrontEnd()
		{
		}

		void FrontEnd::pre_render()
		{
			// sort nodes by shader and texture
		}

		void FrontEnd::post_render()
		{
			// render it all
			m_render_dispatcher->process();
		}

		void FrontEnd::render()
		{
			// fill queue

			// add queue to dispatcher
			m_render_dispatcher->add_queue(&m_render_queue);
		}

		void FrontEnd::set_camera(scene::Camera* camera)
		{
			m_camera = camera;
		}

		scene::Camera* FrontEnd::get_camera()
		{
			return m_camera;
		}
	}
}
