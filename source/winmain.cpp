// WinMain.cpp 

#include "stdafx.h"
#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>
#include <helium/service_locator.hpp>
#include <application.hpp>
#include "vld.h"

#define IDI_HELIUM 107
#define IDI_SMALL  108
#define WNDCLSNAME L"kWndCls"

using namespace helium;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	(void)hPrevInst;
	(void)lpCmdLine;
#if !defined(NDEBUG)
	Debug debug;
#endif
	const int width = 1280, height = 720;

	// create window class for registration
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = ::LoadIcon(wc.hInstance, MAKEINTRESOURCE(IDI_HELIUM));
	wc.hIconSm = ::LoadIcon(wc.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	wc.hCursor = ::LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)::GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = WNDCLSNAME;

	if (!::RegisterClassEx(&wc)) 
	{
		Debug::write(EDebugLevel::Error, "could not register window class!");
		MessageBoxA(NULL, "could not register window class", "Error!", MB_OK|MB_ICONERROR);
		return 1;
	}
	Debug::write(EDebugLevel::Info, "window registered.");

	DWORD style = (WS_OVERLAPPEDWINDOW & ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX));
	RECT rc = { 0, 0, width, height };
	::AdjustWindowRect(&rc, style, FALSE);

	RECT deskrect;
	HWND desktop = GetDesktopWindow();
	GetWindowRect(desktop, &deskrect);
	int centerx = deskrect.right / 2 - ((rc.right - rc.left) / 2);
	int centery = deskrect.bottom / 2 - ((rc.bottom - rc.top) / 2);

	// create the main window
	HWND hWnd = ::CreateWindow(
		WNDCLSNAME, L"Helium", style,
		centerx, centery,
		rc.right - rc.left, rc.bottom - rc.top,
		NULL, NULL, hInstance, NULL);
	if (hWnd == NULL) 
	{
		Debug::write(EDebugLevel::Error, "could not create window!");
		MessageBoxA(NULL, "could not create a window", "Error!", MB_OK | MB_ICONERROR);
		return 1;
	}

	Debug::write(EDebugLevel::Info, "window created.");

	::ShowWindow(hWnd, nCmdShow);
	::UpdateWindow(hWnd);

	Application app;
	app.initialize(hWnd, width, height);

	MSG msg = { 0 };
	while (msg.message != WM_QUIT) 
	{
		if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
		else 
		{
			if (!app.process())
				break;
			::Sleep(2);
		}
	}

	app.shutdown();

	::DestroyWindow(hWnd);
	Debug::write(EDebugLevel::Info, "window destroyed.");
	Debug::write(EDebugLevel::Info, "exiting.");
	
	return 0;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	helium::OSEventDispatcher* dispatcher = helium::ServiceLocator<helium::OSEventDispatcher>::get_service();
	if (!dispatcher)
		return ::DefWindowProc(hWnd, uMsg, wParam, lParam);

	switch (uMsg)
	{
	case WM_MOUSEMOVE:
	{
		OSEvent event(EOSEventType::MouseMove);
		event.m_data.mouse.move.x = GET_X_LPARAM(lParam);
		event.m_data.mouse.move.y = GET_Y_LPARAM(lParam);
		dispatcher->queue(event);
		break;
	}
	case WM_LBUTTONDOWN:
	{
		OSEvent event(EOSEventType::MouseButton);
		event.m_data.mouse.button.button = EMouseButton::Left;
		event.m_data.mouse.button.state = true;
		dispatcher->queue(event);
		break;
	}
	case WM_LBUTTONUP:
	{
		OSEvent event(EOSEventType::MouseButton);
		event.m_data.mouse.button.button = EMouseButton::Left;
		event.m_data.mouse.button.state = false;
		dispatcher->queue(event);
		break;
	}
	case WM_MBUTTONDOWN:
	{
		OSEvent event(EOSEventType::MouseButton);
		event.m_data.mouse.button.button = EMouseButton::Middle;
		event.m_data.mouse.button.state = true;
		dispatcher->queue(event);
		break;
	}
	case WM_MBUTTONUP:
	{
		OSEvent event(EOSEventType::MouseButton);
		event.m_data.mouse.button.button = EMouseButton::Middle;
		event.m_data.mouse.button.state = false;
		dispatcher->queue(event);
		break;
	}
	case WM_RBUTTONDOWN:
	{
		OSEvent event(EOSEventType::MouseButton);
		event.m_data.mouse.button.button = EMouseButton::Right;
		event.m_data.mouse.button.state = true;
		dispatcher->queue(event);
		break;
	}
	case WM_RBUTTONUP:
	{
		OSEvent event(EOSEventType::MouseButton);
		event.m_data.mouse.button.button = EMouseButton::Right;
		event.m_data.mouse.button.state = false;
		dispatcher->queue(event);
		break;
	}
	case WM_XBUTTONDOWN:
	{
		OSEvent event(EOSEventType::MouseButton);
		event.m_data.mouse.button.button = EMouseButton::Extra1;
		event.m_data.mouse.button.state = true;
		dispatcher->queue(event);
		break;
	}
	case WM_XBUTTONUP:
	{
		OSEvent event(EOSEventType::MouseButton);
		event.m_data.mouse.button.button = EMouseButton::Left;
		event.m_data.mouse.button.state = false;
		dispatcher->queue(event);
		break;
	}
	case WM_MOUSEWHEEL:
	{
		OSEvent event(EOSEventType::MouseWheel);
		event.m_data.mouse.wheel.delta = (float)HIWORD(wParam) / (float)WHEEL_DELTA;
		dispatcher->queue(event);
		break;
	}
	case WM_KEYDOWN:
	{
		OSEvent event(EOSEventType::Keyboard);
		event.m_data.keyboard.keycode = (unsigned)wParam;
		event.m_data.keyboard.state = true;
		dispatcher->queue(event);
		break;
	}
	case WM_KEYUP:
	{
		OSEvent event(EOSEventType::Keyboard);
		event.m_data.keyboard.keycode = (unsigned)wParam;
		event.m_data.keyboard.state = false;
		dispatcher->queue(event);
		break;
	}
	case WM_CHAR:
	case WM_UNICHAR:
	{
		OSEvent event(EOSEventType::Text);
		event.m_data.text.unicode = (unsigned)wParam;
		dispatcher->queue(event);
		break;
	}
	case WM_SIZE:
	{
		OSEvent event(EOSEventType::Resize);
		event.m_data.resize.width = LOWORD(lParam);
		event.m_data.resize.height = HIWORD(lParam);
		dispatcher->queue(event);
		break;
	}
	case WM_SETFOCUS:
	{
		OSEvent event(EOSEventType::Focus);
		event.m_data.focus.state = true;
		dispatcher->queue(event);
		break;
	}
	case WM_KILLFOCUS:
	{
		OSEvent event(EOSEventType::Focus);
		event.m_data.focus.state = false;
		dispatcher->queue(event);
		break;
	}
	case WM_CLOSE:
		PostQuitMessage(0);
		break;
	default:
		return ::DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0;
}
